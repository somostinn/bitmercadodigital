<?php 
get_header();
//setPostViews(get_the_ID());
$categories = get_the_category();
$category = $categories[0];

//echo '<pre>';var_dump($category);echo '</pre>';die();


if ($category->slug == 'productos'):
    get_template_part('content', 'tpl-productos');

else :
    get_template_part('content', 'tpl-base');
endif;
?>
<!-- Archivo de pié global de Wordpress -->
<?php get_footer(); 
