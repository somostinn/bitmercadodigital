
<section class="section">
    <div class="shop-categories">
        <div class="masonry grid grid-4">
            <?php
            $postsSlider = new WP_Query(array('category_name' => 'productos', 'order' => 'ASC', 'posts_per_page' => '-1'));
            $i = 0;
            while ($postsSlider->have_posts()) : $postsSlider->the_post();
                $classI = get_post_meta(get_the_ID(), 'class', true);
                ?>           
                <div class="item  <?php echo $classI; ?>">
                    <div class="item-wrapper">
                        <a href="<?php echo get_post_permalink(); ?>">
                            <figure class="he-center">
                                <img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" />
                                <figcaption>
                                    <h2 class="t-important t-center"><span></span></h2>
                                    <p class="category-desc">Ver Detalle</p>
                                </figcaption>
                            </figure>
                        </a>
                    </div>
                </div>   
                <?php
                $i++;
            endwhile;
            wp_reset_query();
            ?>       
        </div>
    </div>
</section>