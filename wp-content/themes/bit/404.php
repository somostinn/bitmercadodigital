<!-- Archivo de cabecera global de Wordpress -->
<?php get_header(); ?>
<section class="section page-404 height-full bg-primary">
        <div class="container">
          <div class="row">
            <div class="col-md-12 m-t-60">
              <h1 class="text-animate zoom">
                <span class="words-wrapper">
                  <b class="is-visible">Parece&nbsp;que&nbsp;se&nbsp;perdió</b>
                  <b>Utiliza&nbsp;la&nbsp;búsqueda&nbsp;para&nbsp;</b>
                  <b>encontrar&nbsp;su&nbsp;camino</b>
                  <b>O&nbsp;enviar&nbsp;un&nbsp;mensaje</b>
                </span>
              </h1>
              <a class="btn btn-hg btn-dark btn-rounded icon-left-effect" href="<?php echo get_site_url(); ?>"><i class="nc-icon-outline ui-1_home-minimal"></i><span>Ir a la página de inicio</span></a>
            </div>
          </div>
        </div>
      </section>
    </div>

    <!-- BEGIN BUILDER TOOLTIP -->
    <div class="tooltip-builder" data-pos-top="90" data-pos-left="30">
      <a href="#" class="tooltip-icon">
        <i class="nc-icon-glyph ui-2_alert-square-i"></i>
      </a>
      <div class="tooltip-popup">
        <div class="tooltip-wrapper">
          <a href="#" class="close-tooltip-popup">
            <i class="nc-icon-outline ui-1_simple-remove"></i>
          </a>
          <p class="tooltip-title">Page variations</p>
          <p>404 page have 3 variations: flat, image background or self hosted video.</p>
          <p>You can embed Youtube or Vimeo video too if you prefer like in coming soon page.</p>
          <div class="flexslider" data-plugin-options='{"controlNav":false,"directionNav": false,"slideshowSpeed":3500}'>
            <ul class="slides">
              <li>
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/demo/layouts/small-404.jpg" alt="&nbsp;">
              </li>
              <li>
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/demo/layouts/small-404-image.jpg" alt="&nbsp;">
              </li>
              <li>
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/demo/layouts/small-404-video.jpg" alt="&nbsp;">
              </li>
            </ul>
          </div>
          <a class="fade-link" href="404-image"><i class="nc-icon-outline arrows-3_circle-simple-right"></i> Try 404 image</a>
          <a class="fade-link" href="404-video"><i class="nc-icon-outline arrows-3_circle-simple-right"></i> Try 404 video</a>
        </div>
      </div>
    </div>
    <!-- END BUILDER TOOLTIP -->
    <!-- Archivo de pié global de Wordpress -->
<?php
get_footer();
