<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'bit');

/** MySQL database username */
define('DB_USER', 'bit');

/** MySQL database password */
define('DB_PASSWORD', '123456');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '@IYy{dS%]Kdz9K{*>7NF9Jb#){=h%38yIob,B,`h.^sMTW5kz<DtBK>1Y9Yqy wR');
define('SECURE_AUTH_KEY',  'Ai~BAdb]{!^Q7Ky>Mp;Def9-4W3<@Fu}j6uWLzje9j%jH/?Mk7tg)bmSo$mG./mX');
define('LOGGED_IN_KEY',    'V;.galle!~Q4o|Oo%ROF tEtiU2tkuaOcEoC$JnR$ZcMF{1xtNSroo/I7Nic!f]3');
define('NONCE_KEY',        ',Nl?2ZHJuouG;EH2[M$xBM?zs?K3g8mFnNNop&8_)iUAvghqy sNpjxtL6|?16Tr');
define('AUTH_SALT',        'DOAG^us%[Rk}5ckKD[Hb6>`FI%:Zb*%~q}tF:^kW:Q=x@s[*%zYeVk0#Xxg2gmf:');
define('SECURE_AUTH_SALT', 'So3M<UsHk&r;JIWQCS!YMo|ip-Z!QDTk15/+=TD$;/2k/keO?Dc dW3FP, d^QCq');
define('LOGGED_IN_SALT',   '2RA={;2+[2M67M7s.v/bPN,!-@7/`0A|JoCtFyn{Np-!w.7qev;C3W71 AUN08H!');
define('NONCE_SALT',       '@Dd+@NI&/**]fSDS)XMia]=GwA~Sv jd<TdK}4/@Ke7O8QQW-6-eW(!|mpcxN@tF');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'bit_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
